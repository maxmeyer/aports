# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=openrc
pkgver=0.44.3
_ver=${pkgver/_git*/}
pkgrel=0
pkgdesc="OpenRC manages the services, startup and shutdown of a host"
url="https://github.com/OpenRC/openrc"
arch="all"
license="BSD-2-Clause"
depends="ifupdown-any"
makedepends="bsd-compat-headers"
checkdepends="sed"
subpackages="$pkgname-doc $pkgname-dev $pkgname-dbg
	$pkgname-zsh-completion:zshcomp:noarch
	$pkgname-bash-completion:bashcomp:noarch"
install="$pkgname.post-install $pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenRC/openrc/archive/$pkgver.tar.gz

	0001-call-sbin-mkmntdirs-in-localmount-OpenRC-service.patch
	0002-fsck-don-t-add-C0-to-busybox-fsck.patch
	0003-force-root-be-rw-before-localmount.patch
	0004-hide-error-when-migrating-var-run-to-run.patch
	0005-rc-pull-in-sysinit-and-boot-as-stacked-levels-when-n.patch
	0006-make-consolefont-service-compatible-with-busyboxs-se.patch
	0007-Fix-undeclared-UT_LINESIZE.patch
	0008-Support-early-loading-of-keymap-if-kbd-is-installed.patch
	0009-rc-mount-make-timeout-invocation-compatible-with-bus.patch
	0010-Add-support-for-starting-services-in-a-specified-VRF.patch
	0011-Clean-up-staticroute-config-remove-irrelevant-parts-.patch

	openrc.logrotate
	hostname.initd
	hwdrivers.initd
	modules.initd
	modloop.initd
	networking.initd
	modloop.confd
	sysfsconf.initd
	firstboot.initd
	sysctl.initd
	machine-id.initd
	test-networking.sh
	"

prepare() {
	default_prepare
	sed -i -e '/^sed/d' "$builddir"/pkgconfig/Makefile
}

build() {
	export MKZSHCOMP=yes
	export MKBASHCOMP=yes
	make LIBEXECDIR=/lib/rc
}

check() {
	make check

	# run unit tests for networking.initd
	#cd "$srcdir"
	#( set -e; sh test-networking.sh )
}

package() {
	local i j

	make LIBEXECDIR=/lib/rc DESTDIR="$pkgdir/" install

	# we cannot have anything turned on by default
	rm -f "$pkgdir"/etc/runlevels/*/*

	# we still use our ifup/ifdown based net config
	rm -f "$pkgdir"/etc/conf.d/network "$pkgdir"/etc/init.d/network

	# our hostname init script reads hostname from /etc/hostname
	rm -f "$pkgdir"/etc/conf.d/hostname

	# we override some of the scripts
	for i in "$srcdir"/*.initd; do
		j=${i##*/}
		install -Dm755 $i "$pkgdir"/etc/init.d/${j%.initd}
	done

	# we override some of the conf.d files
	for i in "$srcdir"/*.confd; do
		j=${i##*/}
		install -Dm644 $i "$pkgdir"/etc/conf.d/${j%.confd}
	done

	# additional documentation considered useful
	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 ChangeLog *.md "$pkgdir"/usr/share/doc/$pkgname/

	# we use a virtual keymaps services to allow users to set their
	# keymaps either with the OpenRC loadkeys service provided by
	# the kbd aport or with the loadkmap service provided by the
	# busybox-initscripts aport.
	rm -f "$pkgdir/etc/init.d/keymaps" \
		"$pkgdir/etc/conf.d/keymaps"

	install -Dm644 "$srcdir/$pkgname.logrotate" "$pkgdir/etc/logrotate.d/$pkgname"
	install -d "$pkgdir"/etc/local.d "$pkgdir"/run

	# openrc upstream removed service(8) for whatever reason, put it back
	ln -s /sbin/rc-service "$pkgdir"/sbin/service
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/bash-completion
	mv "$pkgdir"/usr/share/bash-completion/completions \
		"$subpkgdir"/usr/share/bash-completion
	rm -rf "$pkgdir"/usr/share/bash-completion
}

zshcomp() {
	depends=""
	pkgdesc="Zsh completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	mkdir -p "$subpkgdir"/usr/share/zsh
	mv "$pkgdir"/usr/share/zsh/site-functions \
		"$subpkgdir"/usr/share/zsh
	rm -rf "$pkgdir"/usr/share/zsh
}

sha512sums="
b066feb89d3b58383d5dc20e731f016f0ecdaa72b95af750e49ba31d80d88b83f3df4d9a7d57c8b106c29cc5b0a004b9eee78192cf776b4cfacdcdcb1d7beeb8  openrc-0.44.3.tar.gz
0d89df1ad6c43a5c49e6ea66bcbb405e42f73d36f3fa7fdf21e37d26b4d2708f32cc33dee8bacc73c668b610051902f84f16b2f9007e1c380f267958dac9ea9c  0001-call-sbin-mkmntdirs-in-localmount-OpenRC-service.patch
97c52ffbb59da6bc2f2358825fa8f9876b35fbf2e52363524ef52bfcf5240edb10e22792ca25d2b0071db74eecef299a0d29b32766d3ce9f931fb613f01656de  0002-fsck-don-t-add-C0-to-busybox-fsck.patch
e6914171a722a871632fe848350f889634b0ad5d83d87bd3580d2e9b3f745d6b4dfe95d1ee99a7c518bdd511e4b098b0e910a7330e3c4c8626f8f1b0575cba8b  0003-force-root-be-rw-before-localmount.patch
cf8bc515d95a8feb59333b0322ec979616d57e8857a1b94308a5e46d3c9e764a0d0ad751a33f47c31e66191dcd41004e83c7a2af7a333c42ea65ed033b1f7cd8  0004-hide-error-when-migrating-var-run-to-run.patch
53459c5f6239d221023f840b0f3a8ce47678cc05b436b1d0b63d9b6974198d2f265e9e484bc0cb17a9c27f46decd2f8ba6c580d59eb6c6dbc176bab0af202f41  0005-rc-pull-in-sysinit-and-boot-as-stacked-levels-when-n.patch
6d279df577dfdf2ed7dc063b55d98b05e80cc3e8ce2ee1923702a7d6c1a6ef709a0f77394af56f57b0abc8b7be48afeee0967b36b1df37080c6c1ac90980b82c  0006-make-consolefont-service-compatible-with-busyboxs-se.patch
f3f9098a5a7c04fc0cb695658031af11663c21b8aa112d4e9a67512eef59decdc385bdfc48a9b27e4a966fe233ceca32905c60f0239426b9993088937cb9b2e4  0007-Fix-undeclared-UT_LINESIZE.patch
b39d74f56c7213309ed981ced1fbeb5d615183a94069b095fbc8f29f2a3815cc6ade3e043af6774022545f881ffd604e4f55560d08da3f7483ce3f8c3b82352e  0008-Support-early-loading-of-keymap-if-kbd-is-installed.patch
ce127fbeef1f51519d090e072b058fbd2e195aed583074e67e4c41d7973ab5c534fa1136e257577907047e61a4c59f599d651fb11b21d2a35a5c30b1eb1cfd39  0009-rc-mount-make-timeout-invocation-compatible-with-bus.patch
7a134e9275887fff21dbba5ada2f9d7497abfaeef2623b407f32b471d24313b786404de06f57e9aeddb256b4d993c1cdb51835c6f0756979f718a651c360e90a  0010-Add-support-for-starting-services-in-a-specified-VRF.patch
cce532a6f7024d6aa4f546e57dfc4f9aa1ba4d9e3418935494b347aaf3da3c2d875e92843427603ab197348a6bcd5ddf25e5fbd334a61b2852e842a6aa5b7e36  0011-Clean-up-staticroute-config-remove-irrelevant-parts-.patch
12bb6354e808fbf47bbab963de55ee7901738b4a912659982c57ef2777fff9a670e867fcb8ec316a76b151032c92dc89a950d7d1d835ef53f753a8f3b41d2cec  openrc.logrotate
493f27d588e64bb2bb542b32493ed05873f4724e8ad1751002982d7b4e07963cfb72f93603b2d678f305177cf9556d408a87b793744c6b7cd46cf9be4b744c02  hostname.initd
c06eac7264f6cc6888563feeae5ca745aae538323077903de1b19102e4f16baa34c18b8c27af5dd5423e7670834e2261e9aa55f2b1ec8d8fdc2be105fe894d55  hwdrivers.initd
7113c930f7f5fb5b345b115db175f8e5837e3541b3e022d5cecf1b59067ed4b40b2adea2324a008035b97d653311217ac5cf961b4d0fc8b714a8b2505883cdc6  modules.initd
61857beb0ce1b462ff4bde595ee3808d12b1c51935e6a6bc263bf26a4adc99b434676277e270d82ed2886ceb9c82cb2a5604887bc25fef20bec223097c4d0ee4  modloop.initd
7883ed880c49db3f7fb7598c8cc01f9830ccb5f70b64ed620213baf4900289a718d89a5f7bf97dc94593465eb0e1e3ed126b19dfeaaf9d03a7c46e4e7b6c4472  networking.initd
80e43ded522e2d48b876131c7c9997debd43f3790e0985801a8c1dd60bc6e09f625b35a127bf225eb45a65eec7808a50d1c08a5e8abceafc61726211e061e0a2  modloop.confd
d76c75c58e6f4b0801edac4e081b725ef3d50a9a8c9bbb5692bf4d0f804af7d383bf71a73d5d03ed348a89741ef0b2427eb6a7cbf5a9b9ff60a240639fa6ec88  sysfsconf.initd
990855f875513a85c2b737685ac5bfdfa86f8dadacf00c1826a456547f99b69d4ecf1b9a09c0ce002f1df618b44b1febabe53f95a2c0cd02b504d565bccb50c8  firstboot.initd
2d5f9f6d41b7c0a8643cfdee1ce3c399bfe4ebff54421f33ab1e74c1c4c1b96a49e54b5cd69f0339a060342e4e5a11067bbff68c39fa487919259d73e8e46ed1  sysctl.initd
35682e1742196133b79e4a0b21fe8df039a982ba4fdd0181b1e3872f3885e40726179d4996fec83a1da11ff314d71f8910609c1c05acb3d0f9b923147e2f1d55  machine-id.initd
af17947aa3954e317dc06580da829200e0b0f2ddc37ce842c3fc7fc0d8ca2f40220e4f4665f61b4b5ec47c96416db0127e2ed979b9421bf21df89d4c4f998b7f  test-networking.sh
"
